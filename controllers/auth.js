const User = require('../models/user');
const bcrypt = require('bcrypt');

module.exports.login = async (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    const rows = await User.findOneByEmail(email);
    
    if (rows.length <= 0) {
        return res.status(400).json({
            message: "email and password doesn't match"
        });
    }
    console.log(rows);
    const user = rows[0];
    
    try {
        const isSame = await bcrypt.compare(password, user.hashpassword);
        if (!isSame) {
            return res.status(400).json({
                message: "email and password doesn't match"
            });
        }
        return res.status(200).send("Success");
    } catch (error) {
        return res.status(500).send();
    }

}

module.exports.register = async (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    const username = req.body.username;
    const image = req.body.image;
    const accessToken = req.body.accessToken;

    try {
        const hashPassword = await bcrypt.hash(password, 12);
        const result = User.register(email, hashPassword, username, image, accessToken);
        result.then((val) => {
            return res.status(201).send(val);
        }).catch((reason) => {
            return res.status(500).json({
                message: "failed to register"
            });
        });
        
    } catch (error) {
        res.status(500).json({
            message: "failed to register"
        });
    }
}