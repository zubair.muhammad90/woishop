const express = require('express');
const app = express();
const cors = require('cors');
const authRoute = require('./routes/auth');

app.use(cors());
app.use(express.json());

app.set('view engine', 'ejs');
app.set('views', 'views');

app.use('/api/v1/auth', authRoute);

app.use((req, res) => {
    res.status(404).render('404');
});

app.listen(3001, () => {
    console.log("Server running on port 3001");
});