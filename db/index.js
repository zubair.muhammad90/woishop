const mysql = require('mysql2');

const pool = mysql.createPool({
    user: "root",
    host: "localhost",
    password: "12345678",
    database: "woishop"
});

const promisePool = pool.promise();

module.exports = promisePool;