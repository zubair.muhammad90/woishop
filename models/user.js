const promisePool = require('../db');

module.exports = class User {
    static async findOneByEmail(email) {
        const sql = `SELECT * FROM users WHERE email = ?`;
        const [rows] = await promisePool.query(sql, [email]);
        return rows;
        // console.log(rows);
        // console.log(fields);
    }

    static async register(email, hashPassword, username, image, accessToken, cb) {
        const sql = `INSERT INTO users (email, hashpassword, username, image, accesstoken) 
            VALUES (?, ?, ?, ?, ?)`;
        await promisePool.query(sql, [email, hashPassword, username, image, accessToken]);
        return {
            email: email,
            username: username,
            image: image
        }
    }

}